#ifndef STACK_H
#define STACK_H
#include "linkedList.h"
typedef struct stack
{
	linkedList* head;
} stack;


void push(stack* s, unsigned int element);

int pop(stack* s);

void initStack(stack* s);

void cleanStack(stack* s);

#endif // STACK_H